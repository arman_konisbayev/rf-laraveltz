@extends('layouts.admin_layout')

@section('title', 'Редактирование изображения')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Редактирование изображения:</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->
            @if (session('success'))
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i>{{ session('success') }}</h4>
                </div>
            @endif
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="{{ route('image.update', $image['id']) }}" method="POST">
                            @csrf
                            @method('PATCH')
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Заголовок</label>
                                    <input name="title" value="{{$image['title']}}" type="text" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Описание</label>
                                    <input name="text" value="{{$image['text']}}" type="text" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="customFile">Изображение</label>
                                    <div class="form-group">
                                        <div class="custom-file">
                                            <input name="file" type="file" class="custom-file-input" id="customFile">
                                            <label class="custom-file-label" for="customFile">{{$image['img']}}</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Категория</label>
                                    <select name="category_id" class="form-control" required>
                                        @foreach ($categories as $category)
                                            <option value="{{ $category['id'] }}">{{ $category['name'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Тэги</label>
                                    <input name="tag" value="{{$image['tag']}}" type="text" class="form-control" required>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Сохранить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <script src="/admin/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="/admin/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- bs-custom-file-input -->
    <script src="/admin/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
    <script>
        $(function () {
            bsCustomFileInput.init();
        });
    </script>
    <!-- /.content -->
@endsection
