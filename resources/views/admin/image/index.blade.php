@extends('layouts.admin_layout')

@section('title', 'Все изображении')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Все изображении</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->
            @if (session('success'))
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i>{{ session('success') }}</h4>
                </div>
            @endif
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-body p-0">
                    <table class="table table-striped projects">
                        <thead>
                        <tr>
                            <th style="width: 5%">
                                ID
                            </th>
                            <th>
                                Заголовок
                            </th>
                            <th>
                                Описание
                            </th>
                            <th>
                                Изображения
                            </th>
                            <th>
                                Категория
                            </th>
                            <th>
                                Тэги
                            </th>
                            <th style="width: 30%">
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($images as $image)
                            <tr>
                                <td>
                                    {{ $image['id'] }}
                                </td>
                                <td>
                                    {{ $image['title'] }}
                                </td>
                                <td>
                                    {{ $image['text'] }}
                                </td>
                                <td>
                                    {{ $image['img'] }}
                                </td>
                                <td>
                                    {{ optional($image->category)->name }}
                                </td>
                                <td>
                                    {{ $image['tag'] }}
                                </td>

                                <td class="project-actions text-right">
                                    <a class="btn btn-primary btn-sm" href="{{ route('image.show',$image['id']) }}">
                                        <i class="fas fa-eye"></i>
                                        </i>
                                        Просмотр
                                    </a>
                                    <a class="btn btn-info btn-sm" href="{{ route('image.edit', $image['id']) }}">
                                        <i class="fas fa-pencil-alt">
                                        </i>
                                        Редактировать
                                    </a>
                                    @include('admin.image._partarials._form')
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
