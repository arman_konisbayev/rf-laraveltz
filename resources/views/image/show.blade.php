@extends('layouts.app')
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div><!-- /.col -->
                <form method="GET" action="{{route('image.show')}}">
                    <div class="row mb-2">
                        <div class="form-group col-sm-6 col-md-3">
                            <!-- select -->
                            <div class="form-group">
                                <label>Категория</label>
                                <select name="category_id" class="form-control">
                                    @foreach ($categories as $category)
                                        <option value="{{ $category['id'] }}">{{ $category['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <label for="text">Описание
                                <input type="text" name="text" id="text" size="6" value="{{ request()->text}}">
                            </label>
                            <label for="tags">Тэги
                                <input type="text" name="tags" id="tags" size="6"  value="{{ request()->tags }}">
                            </label>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <button type="submit" class="btn btn-primary">Применить фильтр</button>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <a href="{{route('image.show')}}" class="btn btn-warning">Сбросить фильтр</a>
                        </div>
                    </div>
                </form>
            </div><!-- /.row -->
            @if (session('success'))
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i>{{ session('success') }}</h4>
                </div>
            @endif
        </div><!-- /.container-fluid -->
    </div>
    <div class="container">
            <section class="content">
                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body p-0">
                            <table class="table">
                                <tbody>
                                @foreach ($images as $image)
                                    <tr>
                                        <td>
                                            <h3>Изображение</h3>
                                            <p style="font-size: 20px">Заголовок: <strong>{{$image->title}}</strong></p>
                                            <img src="{{ Storage::url($image->img)}}" style="height: 240px">
                                            <p style="font-size: 20px">Описание: <strong>{{$image->text}}</strong></p>
                                            <p style="font-size: 20px">Категория: <strong>  {{ optional($image->category)->name }}</strong></p>
                                            <p style="font-size: 20px">Тэги: <strong>  {{ $image->tag }}</strong></p>
                                        </td>
                                    </tr>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>
        {{$images->links()}}
        <style>
            .w-5{
                display: none;
            }
        </style>
    </div>
@endsection
