<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 *
 * @mixin \Eloquent
 * @property string             $img
 * @property string             $title
 * @property string             $text
 * @property int                $category_id
 * @property int                $tag
 * @property Carbon             $created_at
 * @property Carbon             $updated_at
 */


class Image extends Model
{
    use HasFactory;

    protected $table = 'images';

    protected $fillable = [
        'img',
        'title',
        'text',
        'category_id',
        'tag'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class,'category_id','id');
    }
}
