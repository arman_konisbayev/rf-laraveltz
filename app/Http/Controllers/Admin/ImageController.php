<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Image;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $images = Image::query()
            ->orderBy('created_at','desc')
            ->get();

        return view('admin.image.index', [
            'images' => $images
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::query()
            ->orderBy('created_at', 'desc')
            ->get();

        return view('admin.image.create', [
            'categories' => $categories
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            [
                'photo' => 'image|mimes:jpeg,png,jpg,svg|max:2048',
            ]
        );
        $path = $request->file('photo')->store('/images');
        $image = new Image();
        $image->title = $request->title;
        $image->text = $request->text;
        $image->img = $path;
        $image->category_id = $request->category_id;
        $image->tag = $request->tag;
        $image->save();
        return redirect()->back()->with('success','Категория успешно создана!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function show(Image $image)
    {
        return view('admin.image.show',
        [
            'image' => $image
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function edit(Image $image)
    {
        $categories = Category::query()
            ->orderBy('created_at', 'desc')
            ->get();

        return view('admin.image.edit', [
            'categories' => $categories,
            'image' => $image,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Image $image)
    {
        $image->img = $request->file;
        $image->title = $request->title;
        $image->text = $request->text;
        $image->category_id = $request->category_id;
        $image->tag = $request->tag;
        $image->save();

        return redirect()->back()->with('success','Категория успешно обновлена!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function destroy(Image $image)
    {
        $image->delete();

        return redirect()->back()->with('success','Категория успешно была удалена!');
    }

    public function views(Request $request)
    {
        $categories = Category::query()
            ->orderBy('created_at', 'desc')
            ->get();

        $imagesQuery = Image::query()->with('category');
        $category_id = $request->get('category_id');
        $text = $request->get('text');
        $tags = $request->get('tags');

        if ($category_id)
        {
            $imagesQuery->where('category_id', '=', $category_id);
        }
        if ($text)
        {
            $imagesQuery->where('text', 'like', '%' .$text. '%');

        }
        if ($tags)
        {
            $imagesQuery->where('tag', 'like', '%' .$tags. '%');
        }

        $images = $imagesQuery->orderBy('created_at','desc')->get();

        return view('admin.image.views',[
            'images' => $images,
            'categories' => $categories
        ]);
    }
}
