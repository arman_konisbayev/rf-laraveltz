<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Image;
use App\Models\Images;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $images_count = Image::all()->count();

        $categories = Category::all()->count();

        return view('admin.home.index',
            [
                'images_count' => $images_count,
                'categories' => $categories
            ]
        );
    }
}
