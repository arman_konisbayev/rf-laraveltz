<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Image;
use \Illuminate\Http\Request;

class ImageController extends Controller
{
    public function show(Request $request)
    {
        $categories = Category::query()
            ->orderBy('created_at', 'desc')
            ->get();

        $imagesQuery = Image::query()->with('category');
        $category_id = $request->get('category_id');
        $text = $request->get('text');
        $tags = $request->get('tags');

        if ($category_id)
        {
            $imagesQuery->where('category_id', '=', $category_id);
        }
        if ($text)
        {
            $imagesQuery->where('text', 'like', '%' .$text. '%');

        }
        if ($tags)
        {
            $imagesQuery->where('tag', 'like', '%' .$tags. '%');
        }

        $images = $imagesQuery->orderBy('created_at','desc')->simplePaginate('2');;

        return view('image.show',[
            'images' => $images,
            'categories' => $categories
        ]);
    }
}
